import fs from 'fs'
import git from 'isomorphic-git'
import http from 'isomorphic-git/http/node'
import { copy, pathExists, readdir, remove } from 'libs/fs'
import { tmpdir } from 'os'
import { join } from 'path'
import prompt from 'prompt'
import { dirs, log, timelog } from '../main'

export const basePull = async (arg: string[]) => {
  const dir = join(tmpdir(), 'andro-base')
  if (await pathExists(dir)) {
    let done = timelog('base', 'Cleaning base dir')
    await remove(dir)
    done()
  }
  log('base', `tempdir: ${dir}`)
  let done = timelog('base', 'Pulling base from bitbucket')

  await git.clone({
    fs,
    http,
    url: 'https://bitbucket.org/andromedia/base',
    dir,
    depth: 1,
  })

  done = timelog('base', 'Updating app to new base...')
  const newdirs = fs.readdirSync(dir)
  for (let i of newdirs) {
    if (i === 'app' || i === '.git' || i === 'node_modules') {
      continue
    }

    if (await pathExists(join(dirs.root, i))) {
      await remove(join(dirs.root, i))
    }
    await copy(join(dir, i), join(dirs.root, i))
  }
  done()
}

const copyPkgs = async (dir: string, to: string) => {
  const subdir = dir.substring(join(dirs.root, 'pkgs').length)
  for (let i of await readdir(dir)) {
    if (i !== 'node_modules' && i !== 'build') {
      await copy(join(dir, i), join(to, 'pkgs', subdir, i))
    }
  }
}

const getGitUserPass = (msg: string) => {
  prompt.message = '  '
  prompt.delimiter = ''
  console.log('\nBitbucket login')
  return new Promise<{ username: string; password: string; msg: string }>(
    (resolve) => {
      prompt.get(
        [
          {
            name: 'username',
            description: '  Username:',
            required: true,
          },
          {
            name: 'password',
            description: '  Password:',
            hidden: true,
            conform: function (value) {
              return true
            },
          },
          {
            name: 'msg',
            default: msg,
            description: '  Commit Message:',
          },
        ],
        function (err, result) {
          resolve(result)
        }
      )
    }
  )
}

const getCommitMsg = async (arg: string[]) => {
  const lastCommit = await git.log({
    fs,
    dir: dirs.root,
    depth: 1,
  })

  let commitMsg = arg.slice(1).join('') || 'fix'
  if (lastCommit.length > 0) {
    commitMsg = lastCommit[0].commit.message
  }
  return commitMsg
}
