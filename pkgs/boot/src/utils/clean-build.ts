import { readdir, remove } from 'libs/fs'
import { join } from 'path'
import { dirs, log } from '../main'

export const cleanBuild = async (
  mode: 'nodb' | 'node_modules' | false = false
) => {
  log('boot', 'Cleaning all build directory...')

  const rm = [] as any
  rm.push(remove(dirs.build))

  if (mode !== 'nodb') {
    for (let [k, v] of Object.entries(dirs.app)) {
      rm.push(remove(join(v, 'build')))
      if (k === 'db') rm.push(remove(join(v, 'node_modules')))
    }
  }

  if (mode === 'node_modules') {
    rm.push(remove(join(dirs.root, 'node_modules')))
  }

  for (let [k, v] of Object.entries(dirs.pkgs)) {
    if (k !== 'web') {
      if (mode === 'node_modules') {
        rm.push(remove(join(v, 'node_modules')))
      }
      rm.push(remove(join(v, 'build')))
    } else {
      const webDirs = await readdir(v)
      for (let d of webDirs) {
        if (mode === 'node_modules') {
          rm.push(remove(join(v, d, 'node_modules')))
        }
        rm.push(remove(join(v, d, 'build')))
      }
    }
  }

  await Promise.all(rm)
}
