import fs from 'fs'
import git from 'isomorphic-git'
import http from 'isomorphic-git/http/node'
import { copy, pathExists, readdir, remove } from 'libs/fs'
import { tmpdir } from 'os'
import { join } from 'path'
import prompt from 'prompt'
import { dirs, log, timelog } from '../main'

export const basePush = async (arg: string[]) => {
  log('base', 'Requesting credentials...')

  const { username, password, msg } = await getGitUserPass(
    (await getCommitMsg(arg)).trim().replace(/(\r\n|\n|\r)/gm, '')
  )
  console.log('')

  const dir = join(tmpdir(), 'andro-base')
  if (await pathExists(dir)) {
    let done = timelog('base', 'Cleaning base dir')
    await remove(dir)
    done()
  }
  log('base', `tempdir: ${dir}`)
  let done = timelog('base', 'Pulling base from bitbucket')

  await git.clone({
    fs,
    http,
    url: 'https://bitbucket.org/andromedia/base',
    dir,
    depth: 1,
    onAuth: (url) => {
      return {
        username,
        password,
      }
    },
  })
  done()

  const files = [
    '.gitignore',
    'package.json',
    'base.js',
    '.prettierrc',
    'pnpm-workspace.yaml',
  ]

  done = timelog('base', 'Copying pkgs')
  for (let [k, v] of Object.entries(dirs.pkgs)) {
    if (k !== 'web') {
      await copyPkgs(v, dir)
    } else {
      const webDirs = await readdir(v)
      for (let d of webDirs) {
        await copyPkgs(join(v, d), dir)
      }
    }
  }

  for (let i of files) {
    await copy(join(dirs.root, i), join(dir, i))
  }
  done()

  done = timelog('base', 'Committing repository')
  const status = await git.statusMatrix({ dir: dir, fs })
  await Promise.all(
    status.map(([filepath, , worktreeStatus]) =>
      worktreeStatus
        ? git.add({ fs, dir, filepath: filepath })
        : git.remove({ fs, dir, filepath: filepath })
    )
  )

  await git.commit({
    fs,
    author: { email: `deploy@andromedia.co.id`, name: username },
    message: msg,
    dir,
  })
  done()

  done = timelog('base', 'Pushing base to bitbucket')
  await git.push({
    fs,
    http,
    url: 'https://bitbucket.org/andromedia/base',
    dir,
    onAuth: (url) => {
      return {
        username,
        password,
      }
    },
  })
  done()
}

const copyPkgs = async (dir: string, to: string) => {
  const subdir = dir.substring(join(dirs.root, 'pkgs').length)
  for (let i of await readdir(dir)) {
    if (i !== 'node_modules' && i !== 'build') {
      await copy(join(dir, i), join(to, 'pkgs', subdir, i))
    }
  }
}

const getGitUserPass = (msg: string) => {
  prompt.message = '  '
  prompt.delimiter = ''
  console.log('\nBitbucket login')
  return new Promise<{ username: string; password: string; msg: string }>(
    (resolve) => {
      prompt.get(
        [
          {
            name: 'username',
            description: '  Username:',
            required: true,
          },
          {
            name: 'password',
            description: '  Password:',
            hidden: true,
            conform: function (value) {
              return true
            },
          },
          {
            name: 'msg',
            default: msg,
            description: '  Commit Message:',
          },
        ],
        function (err, result) {
          resolve(result)
        }
      )
    }
  )
}

const getCommitMsg = async (arg: string[]) => {
  const lastCommit = await git.log({
    fs,
    dir: dirs.root,
    depth: 1,
  })

  let commitMsg = arg.slice(1).join('') || 'fix'
  if (lastCommit.length > 0) {
    commitMsg = lastCommit[0].commit.message
  }
  return commitMsg
}
