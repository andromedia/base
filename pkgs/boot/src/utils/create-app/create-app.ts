import { dirs } from 'boot/src/main'
import { ensureDir, writeFile } from 'libs/fs'
import { join } from 'path'
import { createDbIndexTs } from './db/index_ts'
import { createDbMainIndexTs } from './db/main_index_ts'
import { createDbPackageJson } from './db/package_json'
import { createDbSchemaPrisma } from './db/schema_prisma'
import { createMobileCapacitorConfigJs } from './mobile/capacitor_config_js'
import { createServerBcryptJs } from './server/bcrypt_js'
import { createServerIndexTs } from './server/index_ts'
import { createServerPackageJson } from './server/server_package_json'
import { createWebExternalTsx } from './web/external_tsx'
import { createWebGlobalTs } from './web/global_ts'
import { createWebIndexCss } from './web/index_css'
import { createWebIndexTs } from './web/index_ts'
import { createWebPackageJson } from './web/package_json'
import { createWebTailwindConfigJs } from './web/tailwind_config_ts copy'

const appStructures: Structure = {
  db: {
    prisma: {
      'schema.prisma': createDbSchemaPrisma,
      'package.json': createDbPackageJson,
      src: {
        'index.ts': createDbIndexTs,
        main: {
          'index.ts': createDbMainIndexTs,
        },
      },
      prisma: {
        'schema.prisma': createDbSchemaPrisma,
      },
    },
  },
  server: {
    'package.json': createServerPackageJson,
    src: {
      'index.ts': createServerIndexTs,
      'bcrypt.js': createServerBcryptJs,
    },
  },
  mobile: {
    'capacitor.config.json': createMobileCapacitorConfigJs,
  },
  web: {
    'package.json': createWebPackageJson,
    'tailwind.config.js': createWebTailwindConfigJs,
    src: {
      components: {},
      base: {
        api: {},
        layout: {},
        page: {},
      },
      'global.ts': createWebGlobalTs,
      'index.css': createWebIndexCss,
      'index.tsx': createWebIndexTs,
      'external.tsx': createWebExternalTsx,
    },
    public: {},
  },
}

export const createApp = async () => {
  await createFlat(appStructures, dirs.root)
}

type IFile = { _isFile: boolean; content: string } | Structure | string
interface Structure extends Record<string, IFile> {}

const createFlat = async (app: Structure, basedir: string) => {
  await ensureDir(basedir)

  for (let [k, v] of Object.entries(app)) {
    if (typeof v === 'object') {
      if (v._isFile) {
        await writeFile(join(basedir, k), v.content)
      } else {
        await createFlat(v as any, join(basedir, k))
      }
    }
  }
}
