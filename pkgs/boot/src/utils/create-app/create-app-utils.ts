export const createFile = (content: string | object) => {
  return {
    _isFile: true,
    content:
      typeof content === 'object' ? JSON.stringify(content, null, 2) : content,
  }
}
