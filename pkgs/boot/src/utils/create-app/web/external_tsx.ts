import { createFile } from '../create-app-utils'

export const createWebExternalTsx =
  createFile(`// make sure to export default component not export const
export default {
  'render-html': () => [import('web-utils/components/RenderHTML')],
  'html-head': () => [import('web-utils/components/HtmlHead')],
  'hello-world': () => [import('web-utils/components/HelloWorld')],
  loading: () => [import('web-crud/src/legacy/Loading')],
  pselect: () => [import('web-crud/src/form/web/fields/PureSelect')],
  admin: () => [import('web-crud/src/CRUD')],
  qform: () => [import('web-crud/src/form/BaseForm')],
  qlist: () => [import('web-crud/src/list/QBaseList')],
  'mobile-layout': () => [import('./components/mobile-layout')],
  'btn-landing': () => [import('./components/btn-landing')],
  landing: () => [import('./components/landing')],
  home: () => [import('./landing/Home')],
}
`)
