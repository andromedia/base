import { createFile } from '../create-app-utils'

export const createWebIndexCss = createFile(`@tailwind base;
@tailwind components;
@tailwind utilities;

html,
body,
#root {
  font-family: 'Segoe UI', 'Segoe UI Web (West European)', 'Segoe UI',
    -apple-system, BlinkMacSystemFont, Roboto, 'Helvetica Neue', sans-serif;
  padding: 0px;
  margin: 0px;
  width: 100%;
  min-height: 100vh;
  display: flex;
  flex-direction: column;
}

#client-root {
  flex: 1;
  display: flex;
}

#client-root > div {
  flex: 1;
}
`)
