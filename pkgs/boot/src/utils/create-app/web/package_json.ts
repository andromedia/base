import { createFile } from '../create-app-utils'

export const createWebPackageJson = createFile({
  name: 'web-app',
  version: '1.0.0',
  private: true,
  dependencies: {
    '@emotion/react': '^11.5.0',
    '@fluentui/react': '^8.27.0',
    framework7: '^6.3.7',
    'framework7-react': '^6.3.7',
    mobx: '^6.1.8',
    'mobx-react-lite': '^3.2.0',
    react: 'beta',
    'react-dom': 'beta',
    tailwindcss: '^2.2.19',
    'web-app': 'workspace:^',
    'web-crud': 'workspace:^',
    'web-init': 'workspace:^',
    'web-utils': 'workspace:^',
    'web-view': 'workspace:^',
  },
  devDependencies: {
    '@types/react': '^17.0.3',
    '@types/react-dom': '^17.0.3',
  },
})
