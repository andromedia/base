import { createFile } from '../create-app-utils'

export const createWebTailwindConfigJs = createFile(`const colors = require('tailwindcss/colors')
delete colors.lightBlue

module.exports = {
  corePlugins: {
    preflight: false,
  },
  purge: {
    enabled: true,
    content: [
      './src/**/*.jsx',
      './src/**/*.tsx',
      './public/**/*.html',
      './src/**/*.html',
      '../../pkgs/web/*/src/**/*.tsx',
    ],
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors,
      backgroundColor: colors,
      textColor: colors,
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
`)
