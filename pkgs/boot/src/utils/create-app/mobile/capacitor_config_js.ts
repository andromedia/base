import { createFile } from '../create-app-utils'

export const createMobileCapacitorConfigJs = createFile({
  appId: 'com.andromedia.base',
  appName: 'Andro Base',
  bundledWebRuntime: true,
  webDir: 'www',
  plugins: {
    SplashScreen: {
      androidScaleType: 'CENTER_CROP',
      splashImmersive: 'false',
      splashFullScreen: 'false',
    },
  },
  ios: {
    contentInset: 'never',
  },
//   server: {
//     url: 'http://192.168.124.89:3200',
//   },
})
