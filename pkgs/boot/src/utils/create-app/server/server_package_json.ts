import { createFile } from '../create-app-utils'

export const createServerPackageJson = createFile({
  name: 'server',
  version: '1.0.0',
  private: true,
  types: './src/index.ts',
  main: './build/index.js',
  dependencies: {
    bcryptjs: '^2.4.3',
  },
})
