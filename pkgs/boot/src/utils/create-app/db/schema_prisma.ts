import { createFile } from '../create-app-utils'

export const createDbSchemaPrisma = createFile(`generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = "postgresql://postgres:andromedia123oke@db.plansys.co:5432/petroport_live?schema=public&pool_timeout=0"
}`)
