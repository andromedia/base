import { createFile } from '../create-app-utils'

export const createDbPackageJson = createFile({
  name: 'db',
  version: '1.0.0',
  private: true,
  scripts: {},
  dependencies: {
    '@prisma/client': '^3.8.1',
  },
  types: './src/index.ts',
  devDependencies: {
    prisma: '^3.8.1',
  },
  main: './build/index.js',
})
