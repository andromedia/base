import { createFile } from '../create-app-utils'

export const createDbMainIndexTs =
  createFile(`import { PrismaClient } from '@prisma/client'
export const main = new PrismaClient({})
`)
