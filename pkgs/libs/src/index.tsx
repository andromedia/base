import type { db as dbImpl } from '../../../app/db/index'
import { prepareDBClient } from './db-client'
export * from './ellapsed-time'
export * from './match-route'
export * from './use-window'
export * from './wait-until'
export const db: typeof dbImpl & { query: (sql: string) => Promise<any[]> } =
  prepareDBClient('main')
