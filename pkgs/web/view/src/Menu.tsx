/** @jsx jsx */
import { css, jsx } from '@emotion/react'
import { Label, SearchBox } from '@fluentui/react'
import { memo, useState } from 'react'
import { BaseWindow } from 'web-init/src/window'
import { useRender } from 'web-utils/src/useRender'
import { fuzzyMatch } from 'web/crud/src/form/web/fields/PureSelect'

type IMenuSingle = {
  idx: number
  title: string
  url?: string
  opened?: boolean
  active?: boolean
  render?: () => void
  siblings: IMenuSingle[]
  children: IMenuSingle[]
  parent: IMenuSingle
}

declare const window: BaseWindow

type IMenuMeta = {
  active: number[]
  renderTimeout: ReturnType<typeof setTimeout>
}

export const Menu = memo(
  ({
    data,
    className,
    style,
  }: {
    data: IMenuSingle[]
    className?: string
    style?: any
  }) => {
    if (!(window as any).menuView) {
      ;(window as any).menuView = {
        active: [],
        renderTimeout: 0 as any,
      }
    }
    const menus = toJS(data)
    const meta = (window as any).menuView
    const _render = useRender()
    const [search, setSearch] = useState('')
    const render = (force?: boolean) => {
      if (force) {
        _render()
        return
      }
      if (meta.renderTimeout) {
        clearTimeout(meta.renderTimeout)
      }
      meta.renderTimeout = setTimeout(() => {
        _render()
      }, 1000)
    }

    const walk = (menus: IMenuSingle[], parent?: IMenuSingle) => {
      for (let menu of menus) {
        menu.active = false
        menu.opened = false
        if (parent) menu.parent = parent
        if (menu.url === location.pathname) {
          menu.active = true
          let cur = menu
          while (cur.parent) {
            cur = cur.parent
            cur.active = true
            cur.opened = true
          }
        }
        if (menu.children) {
          walk(menu.children, menu)
        }
      }
    }
    walk(menus)

    return (
      <div className="flex flex-1 flex-col self-stretch" style={style}>
        <SearchBox
          placeholder="Search Menu"
          disableAnimation={true}
          value={search}
          onChange={(_, value) => {
            setSearch(value || '')
          }}
          className="searchbox"
          css={css`
            background: none;
            border:0px;
            .ms-SearchBox-iconContainer {
              display: none;
            }
          `}
        />

        <div
          className={`${
            className || ''
          } relative self-stretch flex flex-1 overflow-auto`}
        >
          <div className="absolute inset-0 menu-container">
            <MenuTree
              menus={menus}
              meta={meta}
              level={0}
              search={search}
              render={render}
            />
          </div>
        </div>
      </div>
    )
  }
)

export const MenuTree = ({
  menus,
  meta,
  parent,
  level,
  search,
  render,
}: {
  level: number
  menus: IMenuSingle[]
  meta: IMenuMeta
  search: string
  parent?: IMenuSingle
  render: (force?: boolean) => void
}) => {
  return (
    <div
      className={`${
        !!search ? 'flatten' : ''
      } menu-tree flex flex-col items-stretch`}
    >
      {menus.map((e, idx) => {
        e.idx = idx
        if (parent) e.parent = parent
        e.siblings = menus

        return (
          <MenuSingle
            key={idx}
            menu={e}
            render={render}
            meta={meta}
            search={search}
            level={level}
          />
        )
      })}
    </div>
  )
}

export const MobileText = (props) => {
  return <div {...props} />
}

export const MenuSingle = ({
  menu,
  meta,
  render,
  level,
  search,
}: {
  level: number
  menu: IMenuSingle
  meta: IMenuMeta
  search: string
  render: (force?: boolean) => void
}) => {
  const Text = window.platform === 'web' ? Label : MobileText
  const _render = useRender()
  menu.render = _render
  let text = (
    <Text
      className={`
      ${
        !!menu.children ? 'has-children' : 'no-children'
      } menu-title cursor-pointer`}
      css={css`
        margin: 0px;
        padding: 0px;
      `}
    >
      {menu.title}
    </Text>
  )
  if (!!search) {
    if (!fuzzyMatch(search, menu.title) || menu.children) text = <></>
  }

  return (
    <div
      className={`menu-item flex flex-col cursor-pointer ${
        menu.active ? 'active' : ''
      } ${!menu.opened && !search ? 'collapsed' : ''}`}
      onClick={(e) => {
        e.preventDefault()
        e.stopPropagation()
        if (menu.children) {
          menu.opened = !menu.opened
          menu.active = !!menu.opened
          let cur = menu
          for (let i of cur.siblings) {
            if (i !== menu) i.opened = false
            if (menu && menu.render) menu.render()
          }
        } else if (menu.url) {
          window.navigate(menu.url)
          render(true)
        }
      }}
    >
      {text}
      <div
        className={`${menu.opened || !!search ? 'flex' : 'hidden'} flex-col`}
      >
        {menu.children && (
          <MenuTree
            menus={menu.children}
            meta={meta}
            search={search}
            parent={menu}
            render={render}
            level={level + 1}
          />
        )}
      </div>
    </div>
  )
}

export default Menu
