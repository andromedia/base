import { useWindow } from 'libs'
import { FC, useEffect, useRef } from 'react'
import { useRender } from 'web-utils/src/useRender'

export const Wrapper: FC = ({ children }) => {
  const { window } = useWindow()
  const _ = useRef({
    Layout: (() => null) as FC,
  })
  const render = useRender()

  useEffect(() => {
    ;(async () => {
      const layout: any =
        window.platform === 'mobile'
          ? (await import('./wrapper-mobile')).default
          : ({ children }) => <>{children}</>

      _.current.Layout = layout
      render()
    })()
  }, [])

  const Layout = _.current.Layout

  return <Layout>{children}</Layout>
}
